package main

import (
	"fmt"
	"os"

	"github.com/charmbracelet/bubbles/key"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/spf13/viper"
)

type model struct {
	State int
	Keys  KeyMap
	viper *viper.Viper
}

// Init is the first function that will be called. It returns an optional
// initial command. To not perform an initial command return nil.
func (m model) Init() tea.Cmd {
	return nil
}

// Update is called when a message is received. Use it to inspect messages
// and, in response, update the model and/or send a command.
func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch {
		case key.Matches(msg, m.Keys.Quit):
			return m, tea.Quit
		}
	}
	return m, nil
}

// View renders the program's UI, which is just a string. The view is
// rendered after every Update.
func (m model) View() string {
	switch m.State {
	case main_menu:
		return fmt.Sprint("Main Menu\n", m.viper.AllSettings())
	}
	return "Oops."
}

type KeyMap struct {
	Quit key.Binding
}

func initialKeyMap() KeyMap {
	return KeyMap{
		Quit: key.NewBinding(
			key.WithKeys("ctrl+c", "q"),
			key.WithHelp("q", "quit"),
		),
	}
}

const (
	main_menu = iota
)

func initialModel() model {
	viper := viper.New()
	viper.SetConfigName("apps")
	viper.SetConfigType("toml")
	viper.AddConfigPath("$HOME/anyder/scratch")
	viper.AddConfigPath("$HOME/.config/amino")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("fatal error config file: %w", err))
	}

	return model{
		State: main_menu,
		Keys:  initialKeyMap(),
		viper: viper,
	}
}

func main() {
	p := tea.NewProgram(initialModel())
	if err := p.Start(); err != nil {
		fmt.Printf("Something went wrong: %v", err)
		os.Exit(1)
	}
}
