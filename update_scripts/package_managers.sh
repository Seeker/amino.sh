#!/bin/bash

Nala() {
  sudo nala upgrade -y
}

Homebrew() {
  brew upgrade
}

Node() {
  npm update --global
}
